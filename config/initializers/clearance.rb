Clearance::SessionsController.layout "login"
Clearance.configure do |config|
  config.mailer_sender = 'reply@example.com'
  config.redirect_url = '/home'
end
