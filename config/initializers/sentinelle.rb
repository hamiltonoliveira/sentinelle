require 'job'
require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.cron '30 0 * * *' do
  Job.execute_jobs
end
