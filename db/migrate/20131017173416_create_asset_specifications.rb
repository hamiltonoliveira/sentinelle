class CreateAssetSpecifications < ActiveRecord::Migration
  def change
    create_table :asset_specifications do |t|
      t.integer     :asset_id, :null => false
      t.string      :description, :null => false
      t.string      :value, :null => false
      t.timestamps
    end
  end
end
