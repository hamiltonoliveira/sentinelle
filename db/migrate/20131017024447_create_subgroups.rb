class CreateSubgroups < ActiveRecord::Migration
  def change
    create_table :subgroups do |t|
      t.integer     :group_id, :null => false
      t.string      :name,  :null => false, :limit => 100
      t.timestamps
    end
  end
end
