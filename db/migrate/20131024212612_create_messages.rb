class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer     :user_id, :null => false
      t.string      :subject, :null => false
      t.text        :body, :null => false
      t.boolean     :read, :null => false, :default => false
      t.integer     :occorrence_id
      t.timestamps
    end
  end
end
