class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer    :enterprise_id, :null => false
      t.string     :name, :null => false
      t.timestamps
    end
  end
end
