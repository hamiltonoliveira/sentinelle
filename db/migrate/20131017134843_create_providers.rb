class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.integer     :enterprise_id, :null => false
      t.string      :name, :null => false, :limit => 255
      t.integer     :kind
      t.string      :contact_name
      t.string      :contact_email
      t.timestamps
    end
  end
end
