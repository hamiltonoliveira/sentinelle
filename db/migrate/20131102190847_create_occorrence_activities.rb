class CreateOccorrenceActivities < ActiveRecord::Migration
  def change
    create_table :occorrence_activities do |t|
      t.integer    :occorrence_id, :null => false
      t.integer    :plan_activity_id, :null => false
      t.integer    :occorrence_asset_id, :null => false
      t.boolean    :executed, :null => false, :default => false
      t.timestamps
    end
  end
end
