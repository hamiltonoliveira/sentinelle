class AddProviderToOccorrence < ActiveRecord::Migration
  def change
    add_column :occorrences, :provider_id, :integer
  end
end