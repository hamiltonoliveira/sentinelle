class AddTechnicalSpecificationsToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :technical_specifications, :text
  end
end