class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.integer     :enterprise_id, :null => false
      t.integer     :group_id, :null => false
      t.integer     :subgroup_id, :null => false
      t.string      :number_of_equity, :limit => 30
      t.string      :name, :limit => 255
      t.string      :description, :limit => 500
      t.date        :acquisition_date
      t.integer     :provider_warranty_id
      t.integer     :provider_technical_assistance_id
      t.date        :warranty_date
      t.integer     :local_id
      t.timestamps
    end
  end
end
