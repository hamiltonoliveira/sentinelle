class AddScheduledToOccorrences < ActiveRecord::Migration
  def change
    add_column :occorrences, :scheduled, :date
  end
end