class AddKindToOccorrences < ActiveRecord::Migration
  def change
    add_column :occorrences, :kind, :integer, :default => 2
  end
end