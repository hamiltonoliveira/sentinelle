class CreatePlanActivities < ActiveRecord::Migration
  def change
    create_table :plan_activities do |t|
      t.integer    :plan_id, :null => false
      t.string     :name, :null => false
      t.integer    :kind, :null => false
      t.string     :unity, :null => false
      t.decimal    :value, :null => false
      t.timestamps
    end
  end
end
