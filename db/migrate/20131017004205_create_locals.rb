class CreateLocals < ActiveRecord::Migration
  def change
    create_table :locals do |t|
      t.string     :name, :null => false, :limit => 100
      t.integer    :enterprise_id, :null => false
      t.timestamps
    end
  end
end
