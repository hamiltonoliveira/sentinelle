class CreateOccorrenceAssets < ActiveRecord::Migration
  def change
    create_table :occorrence_assets do |t|
      t.integer     :occorrence_id, :null => false
      t.integer     :asset_id, :null => false
      t.text        :description
      t.timestamps
    end
  end
end
