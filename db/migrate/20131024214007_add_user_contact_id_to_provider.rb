class AddUserContactIdToProvider < ActiveRecord::Migration
  def change
    add_column :providers, :user_contact_id, :integer
  end
end