class CreateOccorrences < ActiveRecord::Migration
  def change
    create_table :occorrences do |t|
      t.integer     :enterprise_id, :null => false
      t.integer     :user_register_id, :null => false
      t.integer     :user_requestor_id
      t.string      :requestor_name, :null => false, :limit => 255
      t.date        :occorrence_date, :null => false
      t.text        :description, :null => false
      t.integer     :status, :null => false
      t.text        :obs
      t.integer     :local_id
      t.timestamps
    end
  end
end
