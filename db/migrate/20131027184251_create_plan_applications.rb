class CreatePlanApplications < ActiveRecord::Migration
  def change
    create_table :plan_applications do |t|
      t.integer    :plan_id, :null => false
      t.integer    :group_id
      t.integer    :subgroup_id
      t.integer    :asset_id
      t.timestamps
    end
  end
end
