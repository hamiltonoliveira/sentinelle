class CreateNextMaintenances < ActiveRecord::Migration
  def change
    create_table :next_maintenances do |t|
      t.integer    :enterprise_id, :null => false
      t.integer    :asset_id, :null => false
      t.date       :next_date, :null => false
      t.integer    :occorrence_id
      t.integer    :plan_activity_id, :null => false
      t.integer    :plan_id, :null => false
      t.timestamps
    end
  end
end
