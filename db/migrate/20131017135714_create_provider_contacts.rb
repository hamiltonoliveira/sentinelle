class CreateProviderContacts < ActiveRecord::Migration
  def change
    create_table :provider_contacts do |t|
      t.integer    :provider_id, :null => false
      t.string     :phone_area, :limit => 3
      t.string     :phone_number, :limit => 10, :null => false
      t.string     :phone_extension, :limit => 4
      t.string     :contact_name, :limit => 50, :null => false
      t.string     :contact_email
      t.string     :contact_department
      t.timestamps
    end
  end
end
