class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.integer     :enterprise_id, :null => false
      t.string      :name, :null => false, :limit => 100
      t.timestamps
    end
  end
end
