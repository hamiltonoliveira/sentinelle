# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131111215803) do

  create_table "asset_specifications", force: true do |t|
    t.integer  "asset_id",    null: false
    t.string   "description", null: false
    t.string   "value",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", force: true do |t|
    t.integer  "enterprise_id",                                null: false
    t.integer  "group_id",                                     null: false
    t.integer  "subgroup_id",                                  null: false
    t.string   "number_of_equity",                 limit: 30
    t.string   "name"
    t.string   "description",                      limit: 500
    t.date     "acquisition_date"
    t.integer  "provider_warranty_id"
    t.integer  "provider_technical_assistance_id"
    t.date     "warranty_date"
    t.integer  "local_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "technical_specifications"
  end

  create_table "enterprises", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.integer  "enterprise_id",             null: false
    t.string   "name",          limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locals", force: true do |t|
    t.string   "name",          limit: 100, null: false
    t.integer  "enterprise_id",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "user_id",                       null: false
    t.string   "subject",                       null: false
    t.text     "body",                          null: false
    t.boolean  "read",          default: false, null: false
    t.integer  "occorrence_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "next_maintenances", force: true do |t|
    t.integer  "enterprise_id",    null: false
    t.integer  "asset_id",         null: false
    t.date     "next_date",        null: false
    t.integer  "occorrence_id"
    t.integer  "plan_activity_id", null: false
    t.integer  "plan_id",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "checked"
  end

  create_table "occorrence_activities", force: true do |t|
    t.integer  "occorrence_id",                       null: false
    t.integer  "plan_activity_id",                    null: false
    t.integer  "occorrence_asset_id",                 null: false
    t.boolean  "executed",            default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "occorrence_assets", force: true do |t|
    t.integer  "occorrence_id", null: false
    t.integer  "asset_id",      null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "occorrences", force: true do |t|
    t.integer  "enterprise_id",                 null: false
    t.integer  "user_register_id",              null: false
    t.integer  "user_requestor_id"
    t.string   "requestor_name",                null: false
    t.date     "occorrence_date",               null: false
    t.text     "description",                   null: false
    t.integer  "status",                        null: false
    t.text     "obs"
    t.integer  "local_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "provider_id"
    t.date     "scheduled"
    t.integer  "kind",              default: 2
  end

  create_table "plan_activities", force: true do |t|
    t.integer  "plan_id",    null: false
    t.string   "name",       null: false
    t.integer  "kind",       null: false
    t.string   "unity",      null: false
    t.decimal  "value",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plan_applications", force: true do |t|
    t.integer  "plan_id",     null: false
    t.integer  "group_id"
    t.integer  "subgroup_id"
    t.integer  "asset_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plans", force: true do |t|
    t.integer  "enterprise_id", null: false
    t.string   "name",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "provider_contacts", force: true do |t|
    t.integer  "provider_id",                   null: false
    t.string   "phone_area",         limit: 3
    t.string   "phone_number",       limit: 10, null: false
    t.string   "phone_extension",    limit: 4
    t.string   "contact_name",       limit: 50, null: false
    t.string   "contact_email"
    t.string   "contact_department"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "providers", force: true do |t|
    t.integer  "enterprise_id",   null: false
    t.string   "name",            null: false
    t.integer  "kind"
    t.string   "contact_name"
    t.string   "contact_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_contact_id"
  end

  create_table "subgroups", force: true do |t|
    t.integer  "group_id",               null: false
    t.string   "name",       limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "email",                          null: false
    t.string   "encrypted_password", limit: 128, null: false
    t.string   "confirmation_token", limit: 128
    t.string   "remember_token",     limit: 128, null: false
    t.integer  "enterprise_id"
    t.string   "name"
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

end
