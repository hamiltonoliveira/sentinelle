# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :plan_activity do
    name "Activity 01"
    kind 2
    unity 3
    value 2
    association :plan, factory: :plan
  end
end
