# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :subgroup do
    name "Subgroup"
    association :group, factory: :group
  end
end
