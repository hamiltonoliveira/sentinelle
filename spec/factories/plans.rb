# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :plan do
    name "Plan"
    association :enterprise, factory: :enterprise
  end
  
  factory :invalid_plan, class: Plan do
    name nil
    association :enterprise, factory: :enterprise
  end
  
  factory :plan_other_enterprise, class: Plan do
    name "Plan other enterprise"
    association :enterprise, factory: :enterprise_2
  end
end
