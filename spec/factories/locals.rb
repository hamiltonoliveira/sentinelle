# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :local do
    name "Local"
    association :enterprise, factory: :enterprise
  end
  
  factory :invalid_local, class: Local do
    name nil
  end
  
  factory :local_other_enterprise, class: Local do
    name "Local other enterprise"
    association :enterprise, factory: :enterprise_2
  end
end
