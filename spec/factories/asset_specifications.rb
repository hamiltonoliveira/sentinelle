# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :asset_specification do
    association :asset, factory: :asset
    description "Peso"
    value "10"
  end
end
