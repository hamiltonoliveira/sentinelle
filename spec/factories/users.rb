
FactoryGirl.define do
  factory :user do
    email "teste@teste.com.br"
    password "123456"
    name "Teste"
    association :enterprise, factory: :enterprise
  end
end