# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group do
    name "Group"
    association :enterprise, factory: :enterprise
  end
  
  factory :invalid_group, class: Group do
    name nil
  end
  
  factory :group_other_enterprise, class: Group do
    name "Group other enterprise"
    association :enterprise, factory: :enterprise_2
  end
end
