# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :provider do
    name "Provider"
    contact_name "Provider Contact Name"
    contact_email "Provider Contact Email"
    kind 1
    association :enterprise, factory: :enterprise
  end
  
  factory :invalid_provider, class: Provider do
    name nil
    contact_name "Contact Name"
    contact_email "Contact Email"
    kind 1
    association :enterprise, factory: :enterprise
  end
  
  factory :provider_other_enterprise, class: Provider do
    name "Provider other enterprise"
    contact_name "Contact Name"
    contact_email "Contact Email"
    kind 1
    association :enterprise, factory: :enterprise_2
  end
  
end
