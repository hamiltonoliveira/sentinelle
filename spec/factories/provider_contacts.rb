# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :provider_contact do
    phone_number "8588997766"
    contact_name "Contact Name"
    association :provider, factory: :provider
  end
end
