# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :enterprise do
    name "Empresa1"
  end
  
  factory :enterprise_2, class: Enterprise do
    name "Empresa2"
  end
end
