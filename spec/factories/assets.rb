# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :asset do
    name "Asset"
    association :group, factory: :group
    association :subgroup, factory: :subgroup
    association :enterprise, factory: :enterprise
  end
  
  factory :asset_other_enterprise, class: Asset do
    name "Asset"
    association :group, factory: :group
    association :subgroup, factory: :subgroup
    association :enterprise, factory: :enterprise_2
  end
  
  factory :invalid_asset, class: Asset do
    name nil
    association :group, factory: :group
    association :subgroup, factory: :subgroup
    association :enterprise, factory: :enterprise
  end
end
