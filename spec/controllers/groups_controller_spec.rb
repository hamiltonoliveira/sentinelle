require 'spec_helper'

describe GroupsController do

  before do
    @enterprise = FactoryGirl.create(:enterprise)
    @user = FactoryGirl.create(:user, enterprise: @enterprise)
    sign_in_as(@user)
  end
  
  describe "GET index" do
    it "assigns all groups as @groups" do
      group = create(:group, enterprise: @enterprise)
      group_enterprise2 = create(:group_other_enterprise)
      get :index
      expect(assigns(:groups)).to match_array([group])
    end
    
    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET show" do
    it "assigns the requested group as @group" do
      group = create(:group)
      get :show, {:id => group}
      expect(assigns(:group)).to eq group
    end
    
    it "renders the :show template" do
      group = create(:group)
      get :show, {:id => group}
      expect(response).to render_template :show
    end
  end

  describe "GET new" do
    it "assigns a new group as @group" do
      get :new
      expect(assigns(:group)).to be_a_new(Group)
    end
    
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET edit" do
    it "assigns the requested group as @group" do
      group = create(:group)
      get :edit, {:id => group}
      expect(assigns(:group)).to eq group
    end
    
    it "renders the :edit template" do
      group = create(:group)
      get :edit, {:id => group}
      expect(response).to render_template :edit
    end
  end
  
  describe "POST create" do    
    context "with valid params" do
      it "save new group in the database" do
        expect{
          post :create, group: attributes_for(:group, enterprise_id: @enterprise.id)
        }.to change(Group, :count).by(1)
      end
      
      it "redirects to groups#index" do
        post :create, group: attributes_for(:group, enterprise_id: @enterprise.id)
        expect(response).to redirect_to groups_path
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved group as @group" do
        expect{
          post :create, group: attributes_for(:invalid_group, enterprise_id: @enterprise.id)
        }.to_not change(Group, :count)
      end

      it "re-renders the 'new' template" do
        post :create, group: attributes_for(:invalid_group, enterprise_id: @enterprise.id)
        expect(response).to render_template :new
      end
    end
  end


  describe "PUT update" do
    before :each do
      @group = create(:group, enterprise: @enterprise)
    end
    
    context "valid attributes" do
      it "located the requested @group" do
        patch :update, id: @group, group: attributes_for(:group, enterprise_id: @enterprise.id)
        expect(assigns(:group)).to eq(@group)
      end
      
      it "changes @group's attributes" do
        patch :update, id: @group, group: attributes_for(:group, enterprise_id: @enterprise.id, name: "Grupo editado")
        @group.reload
        expect(@group.name).to eq("Grupo editado")
      end
      
      it "redirects to the updated contact" do
        patch :update, id: @group, group: attributes_for(:group, enterprise_id: @enterprise.id)
        expect(response).to redirect_to groups_path    
      end
    end
    
    context "with invalid attributes" do
      it "does not change the group's attributes" do
        patch :update, id: @group, group: attributes_for(:group, enterprise_id: @enterprise.id, name: nil)
        @group.reload
        expect(@group.name).to eq("Group")
      end
      
      it "re-renders the edit template" do
        patch :update, id: @group, group: attributes_for(:group, enterprise_id: @enterprise.id, name: nil)
        expect(response).to render_template :edit
      end
    end
  end
  
  describe "DELETE destroy" do
    before :each do
      @group = create(:group)
    end
    
    it "deletes the group" do
      expect {
        delete :destroy, {:id => @group}
      }.to change(Group, :count).by(-1)
    end

    it "redirects to groups#index" do
      delete :destroy, {:id => @group}
      expect(response).to redirect_to groups_path
    end
  end
  
end
