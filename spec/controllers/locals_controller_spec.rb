require 'spec_helper'


describe LocalsController do

  before do
    @enterprise = FactoryGirl.create(:enterprise)
    @user = FactoryGirl.create(:user, enterprise: @enterprise)
    sign_in_as(@user)
  end
  
  describe "GET index" do
    it "assigns all locals as @locals" do
      local = create(:local, enterprise: @enterprise)
      local_enterprise2 = create(:local_other_enterprise)
      get :index
      expect(assigns(:locals)).to match_array([local])
    end
    
    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET show" do
    it "assigns the requested local as @local" do
      local = create(:local)
      get :show, {:id => local}
      expect(assigns(:local)).to eq local
    end
    
    it "renders the :show template" do
      local = create(:local)
      get :show, {:id => local}
      expect(response).to render_template :show
    end
  end

  describe "GET new" do
    it "assigns a new local as @local" do
      get :new
      expect(assigns(:local)).to be_a_new(Local)
    end
    
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET edit" do
    it "assigns the requested local as @local" do
      local = create(:local)
      get :edit, {:id => local}
      expect(assigns(:local)).to eq local
    end
    
    it "renders the :edit template" do
      local = create(:local)
      get :edit, {:id => local}
      expect(response).to render_template :edit
    end
  end
  
  describe "POST create" do    
    context "with valid params" do
      it "save new local in the database" do
        expect{
          post :create, local: attributes_for(:local, enterprise_id: @enterprise.id)
        }.to change(Local, :count).by(1)
      end
      
      it "redirects to locals#index" do
        post :create, local: attributes_for(:local, enterprise_id: @enterprise.id)
        expect(response).to redirect_to locals_path
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved local as @local" do
        expect{
          post :create, local: attributes_for(:invalid_local, enterprise_id: @enterprise.id)
        }.to_not change(Local, :count)
      end

      it "re-renders the 'new' template" do
        post :create, local: attributes_for(:invalid_local, enterprise_id: @enterprise.id)
        expect(response).to render_template :new
      end
    end
  end


  describe "PUT update" do
    before :each do
      @local = create(:local, enterprise: @enterprise)
    end
    
    context "valid attributes" do
      it "located the requested @local" do
        patch :update, id: @local, local: attributes_for(:local, enterprise_id: @enterprise.id)
        expect(assigns(:local)).to eq(@local)
      end
      
      it "changes @local's attributes" do
        patch :update, id: @local, local: attributes_for(:local, enterprise_id: @enterprise.id, name: "Local editado")
        @local.reload
        expect(@local.name).to eq("Local editado")
      end
      
      it "redirects to the updated contact" do
        patch :update, id: @local, local: attributes_for(:local, enterprise_id: @enterprise.id)
        expect(response).to redirect_to locals_path    
      end
    end
    
    context "with invalid attributes" do
      it "does not change the local's attributes" do
        patch :update, id: @local, local: attributes_for(:local, enterprise_id: @enterprise.id, name: nil)
        @local.reload
        expect(@local.name).to eq("Local")
      end
      
      it "re-renders the edit template" do
        patch :update, id: @local, local: attributes_for(:local, enterprise_id: @enterprise.id, name: nil)
        expect(response).to render_template :edit
      end
    end
  end
  
  describe "DELETE destroy" do
    before :each do
      @local = create(:local)
    end
    
    it "deletes the local" do
      expect {
        delete :destroy, {:id => @local}
      }.to change(Local, :count).by(-1)
    end

    it "redirects to locals#index" do
      delete :destroy, {:id => @local}
      expect(response).to redirect_to locals_path
    end
  end

end
