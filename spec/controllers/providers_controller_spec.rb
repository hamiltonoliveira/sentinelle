require 'spec_helper'

describe ProvidersController do
  
  before do
    @enterprise = FactoryGirl.create(:enterprise)
    @user = FactoryGirl.create(:user, enterprise: @enterprise)
    sign_in_as(@user)
  end
  
  describe "GET index" do
    it "assigns all providers as @providers" do
      provider = create(:provider, enterprise: @enterprise)
      provider_enterprise2 = create(:provider_other_enterprise)
      get :index
      expect(assigns(:providers)).to match_array([provider])
    end
    
    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET show" do
    it "assigns the requested provider as @provider" do
      provider = create(:provider)
      get :show, {:id => provider}
      expect(assigns(:provider)).to eq provider
    end
    
    it "renders the :show template" do
      provider = create(:provider)
      get :show, {:id => provider}
      expect(response).to render_template :show
    end
  end

  describe "GET new" do
    it "assigns a new provider as @provider" do
      get :new
      expect(assigns(:provider)).to be_a_new(Provider)
    end
    
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET edit" do
    it "assigns the requested provider as @provider" do
      provider = create(:provider)
      get :edit, {:id => provider}
      expect(assigns(:provider)).to eq provider
    end
    
    it "renders the :edit template" do
      provider = create(:provider)
      get :edit, {:id => provider}
      expect(response).to render_template :edit
    end
  end
  
  describe "POST create" do
    before :each do
      @contacts = [
        attributes_for(:provider_contact),
        attributes_for(:provider_contact),
        attributes_for(:provider_contact)
      ]
    end
    context "with valid params" do
      it "save new provider in the database" do
        expect{
          post :create, provider: attributes_for(:provider, enterprise_id: @enterprise.id, provider_contacts_attributes: @contacts)
        }.to change(Provider, :count).by(1)
      end
      
      it "redirects to provider#index" do
        post :create, provider: attributes_for(:provider, enterprise_id: @enterprise.id, provider_contacts_attributes: @contacts)
        expect(response).to redirect_to providers_path
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved provider as @provider" do
        expect{
          post :create, provider: attributes_for(:invalid_provider, enterprise_id: @enterprise.id, provider_contacts_attributes: @contacts)
        }.to_not change(Local, :count)
      end

      it "re-renders the 'new' template" do
        post :create, provider: attributes_for(:invalid_provider, enterprise_id: @enterprise.id, provider_contacts_attributes: @contacts)
        expect(response).to render_template :new
      end
    end
  end
  
  describe "PUT update" do
    before :each do
      @provider = create(:provider, enterprise: @enterprise)
    end
    
    context "valid attributes" do
      it "provider the requested @provider" do
        patch :update, id: @provider, provider: attributes_for(:provider, enterprise_id: @enterprise.id)
        expect(assigns(:provider)).to eq(@provider)
      end
      
      it "changes @provider's attributes" do
        patch :update, id: @provider, provider: attributes_for(:provider, enterprise_id: @enterprise.id, name: "Provider editado")
        @provider.reload
        expect(@provider.name).to eq("Provider editado")
      end
      
      it "redirects to the updated contact" do
        patch :update, id: @provider, provider: attributes_for(:provider, enterprise_id: @enterprise.id)
        expect(response).to redirect_to providers_path    
      end
    end
    
    context "with invalid attributes" do
      it "does not change the provider's attributes" do
        patch :update, id: @provider, provider: attributes_for(:provider, enterprise_id: @enterprise.id, name: nil)
        @provider.reload
        expect(@provider.name).to eq("Provider")
      end
      
      it "re-renders the edit template" do
        patch :update, id: @provider, provider: attributes_for(:provider, enterprise_id: @enterprise.id, name: nil)
        expect(response).to render_template :edit
      end
    end
  end

end
