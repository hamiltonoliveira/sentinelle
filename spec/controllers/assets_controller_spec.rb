require 'spec_helper'

describe AssetsController do

  before do
    @enterprise = FactoryGirl.create(:enterprise)
    @user = FactoryGirl.create(:user, enterprise: @enterprise)
    sign_in_as(@user)
  end
  
  describe "GET index" do
    it "assigns all assets as @asset" do
      asset = create(:asset, enterprise: @enterprise)
      asset_enterprise2 = create(:asset_other_enterprise)
      get :index
      expect(assigns(:assets)).to match_array([asset])
    end
    
    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end
  
  describe "GET show" do
    it "assigns the requested asset as @asset" do
      asset = create(:asset)
      get :show, {:id => asset}
      expect(assigns(:asset)).to eq asset
    end
    
    it "renders the :show template" do
      asset = create(:asset)
      get :show, {:id => asset}
      expect(response).to render_template :show
    end
  end
  
  describe "GET new" do
    it "assigns a new asset as @asset" do
      get :new
      expect(assigns(:asset)).to be_a_new(Asset)
    end
    
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end
  
  
  describe "GET edit" do
    it "assigns the requested asset as @asset" do
      asset = create(:asset)
      get :edit, {:id => asset}
      expect(assigns(:asset)).to eq asset
    end
    
    it "renders the :edit template" do
      asset = create(:asset)
      get :edit, {:id => asset}
      expect(response).to render_template :edit
    end
  end
  
  describe "POST create" do
    before :each do
      @group = create(:group, enterprise: @enterprise)
      @subgroup = create(:subgroup, group: @group)
      @specification = [
        attributes_for(:asset_specification)
      ]
    end
    
    context "with valid params" do
      it "save new asset in the database" do
        expect{
          post :create, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id)
          }.to change(Asset, :count).by(1)
      end
      
      it "redirects to asset#index" do
         post :create, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id)
        expect(response).to redirect_to assets_path
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved asset as @asset" do
        expect{
          post :create, asset: attributes_for(:invalid_asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id)
        }.to_not change(Asset, :count)
      end

      it "re-renders the 'new' template" do
        post :create, asset: attributes_for(:invalid_asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id)
        expect(response).to render_template :new
      end
    end
  end
  
  describe "PUT update" do
    before :each do
      @group = create(:group, enterprise: @enterprise)
      @subgroup = create(:subgroup, group: @group)
      @asset = create(:asset, enterprise: @enterprise, group: @group, subgroup: @subgroup)
    end
    
    context "valid attributes" do
      it "provider the requested @asset" do
        patch :update, id: @asset, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id)
        expect(assigns(:asset)).to eq(@asset)
      end
      
      it "changes @asset's attributes" do
        patch :update, id: @asset, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id, name: "Asset Editado")
        @asset.reload
        expect(@asset.name).to eq("Asset Editado")
      end
      
      it "redirects to the updated asset" do
        patch :update, id: @asset, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id, name: "Asset Editado")
        expect(response).to redirect_to assets_path    
      end
    end
    
    context "with invalid attributes" do
      it "does not change the provider's attributes" do
        patch :update, id: @asset, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id, name: nil)
        @asset.reload
        expect(@asset.name).to eq("Asset")
      end
      
      it "re-renders the edit template" do
        patch :update, id: @asset, asset: attributes_for(:asset, enterprise_id: @enterprise.id, group_id: @group.id, subgroup_id: @subgroup.id, name: nil)
        expect(response).to render_template :edit
      end
    end
  end
  
  describe "DELETE destroy" do
    before :each do
      @asset = create(:asset)
    end
    
    it "deletes the group" do
      expect {
        delete :destroy, {:id => @asset}
      }.to change(Asset, :count).by(-1)
    end

    it "redirects to groups#index" do
      delete :destroy, {:id => @asset}
      expect(response).to redirect_to assets_path
    end
  end
  
end
