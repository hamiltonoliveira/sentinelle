require 'spec_helper'

describe PlansController do
  
  before do
    @enterprise = FactoryGirl.create(:enterprise)
    @user = FactoryGirl.create(:user, enterprise: @enterprise)
    sign_in_as(@user)
  end
  
  describe "GET index" do
    it "assigns all plan as @plan" do
      plan = create(:plan, enterprise: @enterprise)
      plan_enterprise2 = create(:plan_other_enterprise)
      get :index
      expect(assigns(:plans)).to match_array([plan])
    end
    
    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET show" do
    it "assigns the requested plan as @plan" do
      plan = create(:plan)
      get :show, {:id => plan}
      expect(assigns(:plan)).to eq plan
    end
    
    it "renders the :show template" do
      plan = create(:plan)
      get :show, {:id => plan}
      expect(response).to render_template :show
    end
  end

  describe "GET new" do
    it "assigns a new plan as @plan" do
      get :new
      expect(assigns(:plan)).to be_a_new(Plan)
    end
    
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "GET edit" do
    it "assigns the requested plan as @plan" do
      plan = create(:plan)
      get :edit, {:id => plan}
      expect(assigns(:plan)).to eq plan
    end
    
    it "renders the :edit template" do
      plan = create(:plan)
      get :edit, {:id => plan}
      expect(response).to render_template :edit
    end
  end
  
  describe "POST create" do
    before :each do
      @activities = [
        attributes_for(:plan_activity)
      ]
      
      @applications = [
        FactoryGirl.build(:plan_application).attributes
      ]
    end
    context "with valid params" do
      it "save new plan in the database" do
        expect{
          post :create, plan: attributes_for(:plan, enterprise_id: @enterprise.id, plan_activities_attributes: @activities, plan_applications_attributes: @applications)
        }.to change(Plan, :count).by(1)
      end
      
      it "redirects to plan#index" do
         post :create, plan: attributes_for(:plan, enterprise_id: @enterprise.id, plan_activities_attributes: @activities, plan_applications_attributes: @applications)
        expect(response).to redirect_to plans_path
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved plan as @plan" do
        expect{
          post :create, plan: attributes_for(:invalid_plan, enterprise_id: @enterprise.id, plan_activities_attributes: @activities, plan_applications_attributes: @applications)
        }.to_not change(Plan, :count)
      end

      it "re-renders the 'new' template" do
        post :create, plan: attributes_for(:invalid_plan, enterprise_id: @enterprise.id, plan_activities_attributes: @activities, plan_applications_attributes: @applications)
        expect(response).to render_template :new
      end
    end
  end
  
  describe "PUT update" do
    before :each do
      @plan = create(:plan, enterprise: @enterprise)
    end
    
    context "valid attributes" do
      it "plan the requested @plan" do
        patch :update, id: @plan, plan: attributes_for(:plan, enterprise_id: @enterprise.id)
        expect(assigns(:plan)).to eq(@plan)
      end
      
      it "changes @plan's attributes" do
        patch :update, id: @plan, plan: attributes_for(:plan, enterprise_id: @enterprise.id, name: "plan editado")
        @plan.reload
        expect(@plan.name).to eq("plan editado")
      end
      
      it "redirects to the updated contact" do
        patch :update, id: @plan, plan: attributes_for(:plan, enterprise_id: @enterprise.id)
        expect(response).to redirect_to plans_path    
      end
    end
    
    context "with invalid attributes" do
      it "does not change the plan's attributes" do
        patch :update, id: @plan, plan: attributes_for(:plan, enterprise_id: @enterprise.id, name: nil)
        @plan.reload
        expect(@plan.name).to eq("Plan")
      end
      
      it "re-renders the edit template" do
        patch :update, id: @plan, plan: attributes_for(:plan, enterprise_id: @enterprise.id, name: nil)
        expect(response).to render_template :edit
      end
    end
  end
  
  describe "DELETE destroy" do
    before :each do
      @plan = create(:plan)
    end
    
    it "deletes the group" do
      expect {
        delete :destroy, {:id => @plan}
      }.to change(Plan, :count).by(-1)
    end

    it "redirects to groups#index" do
      delete :destroy, {:id => @plan}
      expect(response).to redirect_to plans_path
    end
  end
  
end
