require 'spec_helper'

describe Asset do
  context "create new plan" do
    it "is valid" do
      expect(FactoryGirl.build(:asset)).to be_valid
    end
    it "is invalid without enterprise" do
      expect(FactoryGirl.build(:asset, enterprise: nil)).to have(1).errors_on(:enterprise_id)
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:asset, name: nil)).to have(1).errors_on(:name)
    end
    it "is invalid without group" do
      expect(FactoryGirl.build(:asset, group: nil)).to have(1).errors_on(:group_id)
    end
    it "is invalid without subgroup" do
      expect(FactoryGirl.build(:asset, subgroup: nil)).to have(1).errors_on(:subgroup_id)
    end
  end
end
