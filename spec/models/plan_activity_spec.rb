require 'spec_helper'

describe PlanActivity do
  context "create new plan activity" do
    it "is valid" do
      expect(FactoryGirl.build(:plan_activity)).to be_valid
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:plan_activity, name: nil)).to have(1).errors_on(:name)
    end
    it "is invalid without kind" do
      expect(FactoryGirl.build(:plan_activity, kind: nil)).to have(1).errors_on(:kind)
    end
    it "is invalid without unity" do
      expect(FactoryGirl.build(:plan_activity, unity: nil)).to have(1).errors_on(:unity)
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:plan_activity, value: nil)).to have(1).errors_on(:value)
    end
  end
end
