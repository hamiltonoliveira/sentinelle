require 'spec_helper'

describe ProviderContact do
  context "add new contact to provider" do
    it "is valid" do
      expect(FactoryGirl.build(:provider_contact)).to be_valid
    end
    it "is invalid without contact name" do
      expect(FactoryGirl.build(:provider_contact, contact_name: nil)).to have(1).errors_on(:contact_name)
    end
    it "is invalid without phone number" do
      expect(FactoryGirl.build(:provider_contact, phone_number: nil)).to have(1).errors_on(:phone_number)
    end
  end
end
