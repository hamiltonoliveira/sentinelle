require 'spec_helper'

describe Provider do
  context "create new provider" do
    it "is valid" do
      expect(FactoryGirl.build(:provider)).to be_valid
    end
    it "is invalid without enterprise" do
      expect(FactoryGirl.build(:provider, enterprise: nil)).to have(1).errors_on(:enterprise_id)
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:provider, name: nil)).to have(1).errors_on(:name)
    end
    it "is invalid without kind" do
      expect(FactoryGirl.build(:provider, kind: nil)).to have(1).errors_on(:kind)
    end
  end
end
