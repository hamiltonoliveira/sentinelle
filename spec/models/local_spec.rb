require 'spec_helper'

describe Local do
  context "create new local" do
    it "is valid" do
      expect(FactoryGirl.build(:local)).to be_valid
    end
    it "is invalid without enterprise" do
      expect(FactoryGirl.build(:local, enterprise: nil)).to have(1).errors_on(:enterprise_id)
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:local, name: nil)).to have(1).errors_on(:name)
    end
  end
end
