require 'spec_helper'

describe Group do
  context "cadastro de novo grupo" do
    it "dados validos" do
      expect(FactoryGirl.build(:group)).to be_valid
    end
    it "sem informar a empresa" do
      expect(FactoryGirl.build(:group, enterprise: nil)).to have(1).errors_on(:enterprise_id)
    end
    it "com nome vazio" do
      expect(FactoryGirl.build(:group, name: nil)).to have(1).errors_on(:name)
    end
  end
end
