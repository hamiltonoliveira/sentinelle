require 'spec_helper'

describe PlanApplication do
  context "create new plan application" do
    it "is valid" do
      expect(FactoryGirl.build(:plan_application)).to be_valid
    end
    it "is invalid without one option selected" do
      expect(FactoryGirl.build(:plan_application, group_id: nil)).to have(1).errors_on(:selected_application)
    end
  end
end
