require 'spec_helper'

describe Plan do
  context "create new plan" do
    it "is valid" do
      expect(FactoryGirl.build(:plan)).to be_valid
    end
    it "is invalid without enterprise" do
      expect(FactoryGirl.build(:plan, enterprise: nil)).to have(1).errors_on(:enterprise_id)
    end
    it "is invalid without name" do
      expect(FactoryGirl.build(:plan, name: nil)).to have(1).errors_on(:name)
    end
  end
end
