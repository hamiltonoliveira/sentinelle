class ProviderContact < ActiveRecord::Base
  validates_presence_of :phone_number
  validates_presence_of :contact_name
  #validates_presence_of :provider_id
  belongs_to :provider
end
