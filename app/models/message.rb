class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :occorrence
  validates_presence_of :user_id
  validates_presence_of :subject
  validates_presence_of :body
  def self.send_message(user_id, subject, body, occorrence_id)
    message = Message.new
    message.user_id = user_id
    message.subject = subject
    message.body = body
    message.occorrence_id = occorrence_id
    message.save
  end
end
