class OccorrenceAsset < ActiveRecord::Base
  attr  :asset_name
  belongs_to :asset
  def asset_name
    self.asset.name if self.asset
  end
end
