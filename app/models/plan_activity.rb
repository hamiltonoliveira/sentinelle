class PlanActivity < ActiveRecord::Base
  belongs_to :plan, :class_name => "Plan", :foreign_key => "plan_id"
  validates_presence_of :name
  validates_presence_of :kind
  validates_presence_of :unity
  validates_presence_of :value
  KIND_READ = 1
  KIND_TIME = 2
  KIND_LIST = [
    ["Leitura", KIND_READ],
    ["Tempo", KIND_TIME]
  ]
  UNITY_KM = 1
  UNITY_DAYS = 2
  UNITY_MONTHS = 3
  UNITY_LIST = [
    ["KM", UNITY_KM],
    ["Meses", UNITY_MONTHS],
    ["Dias", UNITY_DAYS]
  ]
  def next_date(date = nil)
    date = Time.new.to_date unless date
    days = 0;
    case self.unity
    when UNITY_DAYS
      days = self.value
    when UNITY_MONTHS
      days = self.value * 30
    end
    date += days
  end
end
