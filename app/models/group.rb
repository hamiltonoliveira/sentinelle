class Group < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :enterprise_id
  belongs_to :enterprise
  has_many :subgroups
  accepts_nested_attributes_for :subgroups, reject_if: lambda { |attributes| attributes['name'].blank? }, allow_destroy: true
end
