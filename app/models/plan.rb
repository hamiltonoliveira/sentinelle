class Plan < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :enterprise_id
  belongs_to :enterprise
  has_many :plan_activities, :class_name => "PlanActivity", :foreign_key => "plan_id"
  has_many :plan_applications, :class_name => "PlanApplication", :foreign_key => "plan_id"
  accepts_nested_attributes_for :plan_activities, :allow_destroy => true#, reject_if: :all_blank
  accepts_nested_attributes_for :plan_applications, :allow_destroy => true#, reject_if: :all_blank
end
