class Asset < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :group_id
  validates_presence_of :subgroup_id
  validates_presence_of :enterprise_id
  belongs_to :provider_warranty, :class_name => "Provider", :foreign_key => "provider_warranty_id"
  belongs_to :provider_technical_assistance, :class_name => "Provider", :foreign_key => "provider_technical_assistance_id"
  belongs_to :group
  belongs_to :subgroup
  belongs_to :local
  belongs_to :enterprise
  has_many :asset_specifications, :class_name => "AssetSpecification", :foreign_key => "asset_id"
  accepts_nested_attributes_for :asset_specifications, reject_if: :all_blank, allow_destroy: true
  scope :search_by_name_or_number, ->(name) { where("name ilike '%?%' or number_of_equity = '?'", name, name) }
end
