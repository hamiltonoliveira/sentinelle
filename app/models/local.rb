class Local < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :enterprise_id
  belongs_to :enterprise
  def to_s
    self.name
  end
end
