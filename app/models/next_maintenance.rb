class NextMaintenance < ActiveRecord::Base
  def self.generate_new_maintenances
    NextMaintenance.update_all("checked = false");
    #consulta plan_applications para pegar a lista completa de ativos
    applications = PlanApplication.all
    applications.each do |app|
      #baseado no campos que estiver preenchido é consultado os ativos
      assets = []
      if app.asset_id
        assets = Asset.where("asset_id = ?", app.asset_id)
      elsif app.subgroup_id
        assets = Asset.where("subgroup_id = ?", app.subgroup_id)
      elsif app.group_id
        assets = Asset.where("group_id = ?", app.group_id)
      end
      activities = PlanActivity.where("plan_id = ?", app.plan_id)
      assets.each do |asset|
        #baseado no plans consultar as atividades
        activities.each do |act|
          maintenance = NextMaintenance.where("asset_id = ? and plan_activity_id = ?", asset.id, act.id).first
          if maintenance.nil? && act.kind = PlanActivity::KIND_TIME
            maintenance = NextMaintenance.new
            maintenance.asset_id = asset.id
            maintenance.plan_activity_id = act.id
            maintenance.plan_id = act.plan_id
            maintenance.enterprise_id = asset.enterprise_id
            maintenance.next_date = act.next_date
            maintenance.checked = true
            maintanance.save
          else
            maintenance.checked = true
            maintenance.save
          end
        end
      end      
    end
    #remove registro não encontrados
    NextMaintenance.destroy_all("checked = false")
  end
end
