# encoding: UTF-8

class Occorrence < ActiveRecord::Base
  before_save :update_status
  after_save :generate_messages
  validates_presence_of :occorrence_date
  validates_presence_of :requestor_name
  validates_presence_of :user_register_id
  validates_presence_of :description
  belongs_to :local
  belongs_to :provider
  has_many :occorrence_assets, :class_name => "OccorrenceAsset", :foreign_key => "occorrence_id"
  accepts_nested_attributes_for :occorrence_assets, :allow_destroy => true #reject_if: :all_blank
  has_many :occorrence_activities, :class_name => "OccorrenceActivity", :foreign_key => "occorrence_id"
  ABERTO = 1
  ENCAMINHADO = 2
  AGENDADO = 3
  EM_EXECUCAO = 4
  ENCERRADO = 5
  CANCELADO = 99
  STATUS_LIST = [ ["Aberto", ABERTO], 
                  ["Encaminhado", ENCAMINHADO], 
                  ["Agendado", AGENDADO],
                  ["Em Execução", EM_EXECUCAO],
                  ["Encerrado", ENCERRADO],
                  ["Cancelado", CANCELADO] 
                ]
  OPEN_STATUS_LIST = [ ABERTO, ENCAMINHADO, AGENDADO, EM_EXECUCAO ]
  PREVENTIV = 1
  CORRECTIVE = 2
  def local_name
    self.local.name if self.local
  end              
  def update_status
    unless self.status == EM_EXECUCAO || self.status == ENCERRADO || self.status == CANCELADO
      if self.provider_id.nil?
        self.status = ABERTO
      end
      if self.provider_id
        self.status = ENCAMINHADO
      end
      if self.scheduled
        self.status = AGENDADO
      end
    end                                                                                                                    
  end
  def status_label
    case self.status
    when ABERTO
      '<span class="label">Aberto</span>'
    when ENCAMINHADO
      '<span class="label label-inverse">Encaminhado</span>'
    when AGENDADO
      '<span class="label label-info">Agendado</span>'
    when EM_EXECUCAO
      '<span class="label label-warning">Em Execução</span>'
    when ENCERRADO
      '<span class="label label-success">Encerrado</span>'
    when CANCELADO
      '<span class="label label-important">Cancelado</span>'
    else
      '<span class="label">Aberto</span>'
    end
  end
  def cancel
    self.status = CANCELADO
    self.save
  end
  def close
    self.status = ENCERRADO
    self.save
  end
  def generate_messages
    subject = "[#{self.id}]Atualização de ocorrência"
    body = %Q[
      A ocorrência número #{self.id} deve uma atualização. Seguem os dados abaixo:
      
      #{self.id}
      #{self.status_label}
      Data de geração: #{self.occorrence_date}
      Local: #{self.local}
      Solicitante: #{self.requestor_name}
      Descrição: 
      #{self.description}
      
      Responsável: #{self.provider}
      Data prevista para o reparo: #{scheduled}
    ]
    Message.send_message(self.user_requestor_id, subject, body, self.id) if self.user_requestor_id
    case self.status
    when ENCAMINHADO, AGENDADO, EM_EXECUCAO
      Message.send_message(self.provider.user_contact_id, subject, body, self.id) if self.provider.user_contact_id
    when ENCERRADO
      Message.send_message(self.provider.user_contact_id, subject, body, self.id) if self.provider.user_contact_id
      Message.send_message(self.user_register_id, subject, body, self.id)
    when CANCELADO
      Message.send_message(self.user_register_id, subject, body, self.id)
    end
  end
  def self.generate_preventive_occorrence(asset)
    ActiveRecord::Base.transaction do
      occorrence = Occorrence.new
      occorrence.enterprise_id = asset.enterprise_id
      occorrence.kind = PREVENTIV
      occorrence.occorrence_date = Time.new.to_date
      occorrence.status = ABERTO
      occorrence.description = "Manutenção preventiva"
      occorrence.requestor_name = 'Sentinelle'
      occorrence.local_id = asset.local_id
      occorrence.user_register_id = 0 #ver isso depois
      occorrence.save
    
      occorrence_asset = OccorrenceAsset.new
      occorrence_asset.occorrence_id = occorrence.id
      occorrence_asset.asset_id = asset.id
      occorrence_asset.save
    
      maintenances = NextMaintenance.where("asset_id = ? and next_date <= ? ", asset.id, Time.new)
      maintenances.each do |man|
        list = OccorrenceActivity.joins(:occorrence, :occorrence_asset).where("occorrences.status in (?) and plan_activity_id = ? and occorrence_assets.asset_id = ?", OPEN_STATUS_LIST, man.plan_activity_id, asset.id)
        if list.empty?
          occorrence_activity = OccorrenceActivity.new
          occorrence_activity.occorrence_id = occorrence.id
          occorrence_activity.plan_activity_id = man.plan_activity_id
          occorrence_activity.occorrence_asset_id = occorrence_asset.id
          occorrence_activity.save
        end
      end
      list = OccorrenceActivity.where("occorrence_id = ?", occorrence.id)
      raise ActiveRecord::Rollback if list.empty?
    end
        
  end
end
