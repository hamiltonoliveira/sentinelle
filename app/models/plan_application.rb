class PlanApplication < ActiveRecord::Base
  attr  :asset_name
  belongs_to :plan, :class_name => "Plan", :foreign_key => "plan_id"
  belongs_to :group, :class_name => "Group", :foreign_key => "group_id"
  belongs_to :subgroup, :class_name => "Subgroup", :foreign_key => "subgroup_id"
  belongs_to :asset, :class_name => "Asset", :foreign_key => "asset_id"
  validate :at_least_one_option_must_be_selected
  def asset_name
    self.asset.name if self.asset
  end
  def at_least_one_option_must_be_selected
    errors.add(:selected_application, "at least one option must be selected") unless group_id || subgroup_id || asset_id
  end
end
