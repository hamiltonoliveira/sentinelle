class Provider < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :kind
  validates_presence_of :enterprise_id
  belongs_to :enterprise
  belongs_to :user_contact, :class_name => "User", :foreign_key => "user_contact_id"
  has_many :provider_contacts
  accepts_nested_attributes_for :provider_contacts,  allow_destroy: true
  INTERNAL = 1
  EXTERNAL = 2
  KIND_LIST = [["Interno", INTERNAL], ["Externo", EXTERNAL]]
  def to_s
    self.name
  end
end
