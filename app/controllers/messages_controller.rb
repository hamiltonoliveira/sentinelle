class MessagesController < ApplicationController
  def index
    @messages = Message.where("user_id = ?", current_user.id)
  end
  
  def show
    @message = Message.find(params[:id])
    @message.read = true
    @message.save
  end
end
