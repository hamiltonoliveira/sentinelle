class AssetsController < ApplicationController
  before_action :set_asset, only: [:show, :edit, :update, :destroy]
  autocomplete :asset, :name, :full => true, :scope => [:search_by_name_or_number]
  # GET /assets
  # GET /assets.json
  def index
    @assets = Asset.where("enterprise_id = ?", current_user.enterprise_id)
  end

  # GET /assets/1
  # GET /assets/1.json
  def show
  end

  # GET /assets/new
  def new
    @asset = Asset.new
    init_collections
  end

  # GET /assets/1/edit
  def edit
    init_collections
  end

  # POST /assets
  # POST /assets.json
  def create
    @asset = Asset.new(asset_params)

    respond_to do |format|
      if @asset.save
        format.html { redirect_to assets_path, notice: 'Asset was successfully created.' }
        format.json { render action: 'show', status: :created, location: @asset }
      else 
        init_collections
        format.html { render action: 'new' }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assets/1
  # PATCH/PUT /assets/1.json
  def update
    respond_to do |format|
      if @asset.update(asset_params)
        format.html { redirect_to assets_path, notice: 'Asset was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assets/1
  # DELETE /assets/1.json
  def destroy
    @asset.destroy
    respond_to do |format|
      format.html { redirect_to assets_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asset
      @asset = Asset.find(params[:id])
    end
    
    def init_collections
      @groups = Group.where("enterprise_id = ?", current_user.enterprise_id)
      @locals = Local.where("enterprise_id = ?", current_user.enterprise_id)
      @providers = Provider.where("enterprise_id = ? and kind = 2", current_user.enterprise_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asset_params
      params.require(:asset).permit(:enterprise_id, :group_id, :subgroup_id, :number_of_equity, :name, :description, :acquisition_date, :provider_warranty_id, :provider_technical_assistance_id, :warranty_date, :local_id, :technical_specifications)
    end
end
