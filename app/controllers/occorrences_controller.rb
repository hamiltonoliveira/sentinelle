class OccorrencesController < ApplicationController
  before_action :set_occorrence, only: [:show, :edit, :update, :destroy]
  autocomplete :user, :name, :full => true
  # GET /occorrences
  # GET /occorrences.json
  def index
    @occorrences = Occorrence.all
  end

  # GET /occorrences/1
  # GET /occorrences/1.json
  def show
  end

  # GET /occorrences/new
  def new
    @occorrence = Occorrence.new
    init_collections
  end

  # GET /occorrences/1/edit
  def edit
    init_collections
  end
  
  def execution
    set_occorrence
    init_collections
    @assets = Asset.where("enterprise_id = ?", current_user.enterprise_id)
  end

  # POST /occorrences
  # POST /occorrences.json
  def create
    @occorrence = Occorrence.new(occorrence_params)
    @occorrence.user_register_id = current_user.id
    respond_to do |format|
      if @occorrence.save
        format.html { redirect_to occorrences_path, notice: 'Occorrence was successfully created.' }
        format.json { render action: 'show', status: :created, location: @occorrence }
      else
        init_collections
        format.html { render action: 'new' }
        format.json { render json: @occorrence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /occorrences/1
  # PATCH/PUT /occorrences/1.json
  def update
    respond_to do |format|
      if @occorrence.update(occorrence_params)
        format.html { redirect_to occorrences_path, notice: 'Occorrence was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @occorrence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /occorrences/1
  # DELETE /occorrences/1.json
  def destroy
    @occorrence.cancel
    respond_to do |format|
      format.html { redirect_to occorrences_url }
      format.json { head :no_content }
    end
  end
  
  def close
    set_occorrence
    @occorrence.close
    respond_to do |format|
      format.html { redirect_to occorrences_url }
      format.json { head :no_content }
    end
  end

  private
    def init_collections
      @locals = Local.where("enterprise_id = ?", current_user.id)
      @providers = Provider.where("enterprise_id = ?", current_user.id)
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_occorrence
      @occorrence = Occorrence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def occorrence_params
      params.require(:occorrence).permit(:enterprise_id, :user_register_id, :user_requestor_id, :requestor_name, :occorrence_date, :description, :status, :obs, :local_id, :scheduled, :provider_id, :occorrence_assets_attributes => [:id, :asset_id, :description])
    end
end
