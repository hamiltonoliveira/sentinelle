class Job
  def self.execute_jobs
    puts "Atualizando proxima datas"
    self.new_maintenances
    puts "Gerando ocorrencias"
    self.generate_maintenances
    puts "Finalizado processo"
  end
  private
  def self.new_maintenances
    NextMaintenance.generate_new_maintenances
  end
  def self.generate_maintenances
    maintenances = NextMaintenance.select(:asset_id).distinct.where("next_date <= ?", Time.new)
    maintenances.each do |man|
      asset = Asset.where("id = ?", man.asset_id).first
      Occorrence.generate_preventive_occorrence(asset)      
    end
  end
end